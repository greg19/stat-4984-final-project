# STAT 4984 Final Project
# Gregory Harrison 
5/1/2022

This repository contains all the code I used to generate the neural networks and graphs shown in my project. 
Files named Network##.ipynb are the files that generated the data corresponding to Network## as defined in Table 3 of my project report. 
NetworkTemplate.ipynb is a file that contains the general structure of all of the Network##.ipynb files and was the egg of them. 
trial.ipynb is where I was playing around with the code for the networks before I created the NetworkTemplate.ipynb file. 
graphs.ipynb contain all of the individual peices of data collected from each of the Network##.ipynb results (can also be seen in table 5 of my report) and has the code that generated each of the graphs shown in the report. 

Lastly I would like to note that portions of my code was derived from pytorch's website: https://pytorch.org/tutorials/beginner/blitz/cifar10_tutorial.html 
